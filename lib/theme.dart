import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color primaryColor = Color(0xff6C5ECF);
Color secondaryColor = Color(0xff38ABBE);
Color alrtColor = Color(0xffED6363);
Color linkColor = Color(0xff2C96F1);
Color backgroundColor1 = Color(0xff1F1D2B);
Color backgroundColor2 = Color(0xff2b2937);
Color backgroundColor3 = Color(0xff242231);
Color backgroundColor4 = Color(0xff252836);
Color primaryTextColor = Color(0xffF1F0F2);
Color subTextColor = Color(0xff504f5E);
Color secondaryTextColor = Color(0xff999999);
Color transparentColor = Colors.transparent;
Color blackColor = Color(0xff2E2E2E);


TextStyle primaryTextStyle = GoogleFonts.poppins(
  color: primaryTextColor,
);

TextStyle subTextStyle = GoogleFonts.poppins(
  color: subTextColor,
);

TextStyle secondaryTextStyle = GoogleFonts.poppins(
  color: secondaryTextColor,
);

TextStyle linkTextStyle = GoogleFonts.poppins(
  color: linkColor,
);

TextStyle purpleTextStyle = GoogleFonts.poppins(
  color: primaryColor,
);

TextStyle blackTextStyle = GoogleFonts.poppins(
  color: blackColor,
);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;