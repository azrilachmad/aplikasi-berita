import 'package:flutter/material.dart';
import 'package:tugasakhir/theme.dart';

class BeritaTahukahKamu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/detail-berita');
      },
      child: Container(
        width: 195,
        height: 215,
        margin: EdgeInsets.only(right: 30),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20), color: primaryTextColor),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 30,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(13),
              child: Image.asset(
                'assets/images/image1.png',
                width: 195,
                height: 90,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Kesehatan',
                    style: secondaryTextStyle.copyWith(
                      fontSize: 12,
                    ),
                  ),
                  Text(
                    'Selalu jaga kesehatan COVID-19',
                    style: blackTextStyle.copyWith(
                      fontWeight: semiBold,
                      fontSize: 15,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    'Selengkapnya...',
                    style: linkTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 10,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
