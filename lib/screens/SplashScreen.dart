import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tugasakhir/theme.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
    void initState() {
      Timer(Duration(seconds: 3), () => Navigator.pushNamed(context, '/get-started'),
      );
      super.initState();
    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor1,
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 250,),
            Container(
              width: 130,
              height: 130,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    'assets/images/flutter.png'
                  )
                )
              ),
            ),
            SizedBox(
              height: 41,
            ),
            Text(
              'Flutter Final Project',
              style: primaryTextStyle.copyWith(
                fontSize: 27,
                fontWeight: medium,
              ),
            )
          ],
        )
      ),
    );
  }
} 