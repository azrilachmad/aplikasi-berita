import 'package:flutter/material.dart';
import 'package:tugasakhir/theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tugasakhir/screens/LoginScreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
}

class ProfileScreen extends StatelessWidget {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    Widget logout() {
      return Container(
        margin: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    width: 35,
                    height: 35,
                    child: IconButton(
                      icon: Image.asset('assets/icons/logout.png'),
                      onPressed: () async {
                        await FirebaseAuth.instance.signOut();
                        return new LoginScreen();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget header() {
      return Container(
        margin: EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'About Me',
                    style: purpleTextStyle.copyWith(
                      fontSize: 30,
                      fontWeight: semiBold,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget profileImage() {
      return Container(
        margin: EdgeInsets.only(
          top: 10,
          left: 20,
          right: 20,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 80,
                    height: 80,
                    child: Image.asset('assets/icons/profile_icon.png'),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget profileName() {
      return Container(
        margin: EdgeInsets.only(
          top: 15,
          left: 20,
          right: 20,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Azril Achmad R',
                    style: purpleTextStyle.copyWith(
                      fontSize: 16,
                      fontWeight: medium,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget profileEmail() {
      return Container(
        margin: EdgeInsets.only(
          top: 6,
          left: 20,
          right: 20,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'azrilachmad4@gmail.com',
                    style: subTextStyle.copyWith(
                      fontSize: 13,
                      fontWeight: regular,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget profilePortofolio() {
      return Container(
        margin: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
        ),
        child: Row(
          children: [
            Expanded(
              child: Container(
                width: 282,
                height: 106,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: primaryTextColor),
                child: Container(
                  margin: EdgeInsets.only(
                    top: 8,
                    left: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          Text(
                            'Portofolio',
                            style: blackTextStyle.copyWith(
                              fontSize: 12,
                              fontWeight: bold,
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 50),
                            width: 30,
                            height: 37,
                            child: Image.asset('assets/images/flutter.png'),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 50),
                            width: 30,
                            height: 37,
                            child: Image.asset('assets/images/flutter.png'),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 50),
                            width: 30,
                            height: 37,
                            child: Image.asset('assets/images/flutter.png'),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 45),
                            child: Text(
                              'Flutter',
                              style: subTextStyle.copyWith(
                                fontSize: 12,
                                fontWeight: regular,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 45),
                            child: Text(
                              'Flutter',
                              style: subTextStyle.copyWith(
                                fontSize: 12,
                                fontWeight: regular,
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              'Flutter',
                              style: subTextStyle.copyWith(
                                fontSize: 12,
                                fontWeight: regular,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget profileContact() {
      return Container(
        margin: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
        ),
        child: Row(
          children: [
            Expanded(
              child: Container(
                width: 282,
                height: 106,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: primaryTextColor),
                child: Container(
                  margin: EdgeInsets.only(
                    top: 8,
                    left: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          Text(
                            'Portofolio',
                            style: blackTextStyle.copyWith(
                              fontSize: 12,
                              fontWeight: bold,
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 8, right: 45),
                            width: 30,
                            height: 37,
                            child: Image.asset('assets/icons/linkedin.png'),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 50),
                            width: 30,
                            height: 37,
                            child: Image.asset('assets/icons/telegram.png'),
                          ),
                          Container(
                            width: 30,
                            height: 37,
                            child: Image.asset('assets/icons/gmail.png'),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 24),
                            child: Text(
                              'LinkedIn',
                              style: subTextStyle.copyWith(
                                fontSize: 12,
                                fontWeight: regular,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 32),
                            child: Text(
                              'Telegram',
                              style: subTextStyle.copyWith(
                                fontSize: 12,
                                fontWeight: regular,
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              'Email',
                              style: subTextStyle.copyWith(
                                fontSize: 12,
                                fontWeight: regular,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget profileCourse() {
      return Container(
        margin: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
        ),
        child: Row(
          children: [
            Expanded(
              child: Container(
                width: 282,
                height: 95,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: primaryTextColor),
                child: Container(
                  margin: EdgeInsets.only(
                    top: 8,
                    left: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          Text(
                            'Portofolio',
                            style: blackTextStyle.copyWith(
                              fontSize: 12,
                              fontWeight: bold,
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            width: 126,
                            height: 40,
                            child: Image.asset('assets/images/sanbercode.png'),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: backgroundColor1,
      body: SafeArea(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
            margin: EdgeInsets.only(top: 10, left: 30, right: 30, bottom: 30),
            child: Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    logout(),
                    header(),
                    profileImage(),
                    profileName(),
                    profileEmail(),
                    profilePortofolio(),
                    profileContact(),
                    profileCourse(),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
