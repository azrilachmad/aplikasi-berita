import 'package:flutter/material.dart';
import 'package:tugasakhir/theme.dart';
import 'package:tugasakhir/widgets/BeritaPopuler.dart';
import 'package:tugasakhir/widgets/BeritaTerbaru.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(
          top: 30,
          left: 30,
          right: 30,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Halo, Azril',
                    style: primaryTextStyle.copyWith(
                      fontSize: 24,
                      fontWeight: semiBold,
                    ),
                  ),
                  Text(
                    'azrilachmad4@gmail.com',
                    style: subTextStyle.copyWith(
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: 54,
              height: 54,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage('assets/icons/profile_icon.png'))),
            )
          ],
        ),
      );
    }

    Widget kategori() {
      return Container(
        margin: EdgeInsets.only(
          top: 30,
        ),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              SizedBox(
                width: 30,
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 10,
                ),
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: primaryColor,
                ),
                child: Text(
                  'Semua Berita',
                  style: primaryTextStyle.copyWith(
                      fontSize: 13, fontWeight: medium),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 10,
                ),
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                    color: subTextColor,
                  ),
                  color: transparentColor,
                ),
                child: Text(
                  'Kuliner',
                  style:
                      subTextStyle.copyWith(fontSize: 13, fontWeight: medium),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 10,
                ),
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                    color: subTextColor,
                  ),
                  color: transparentColor,
                ),
                child: Text(
                  'Olahraga',
                  style:
                      subTextStyle.copyWith(fontSize: 13, fontWeight: medium),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 10,
                ),
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                    color: subTextColor,
                  ),
                  color: transparentColor,
                ),
                child: Text(
                  'Politik',
                  style:
                      subTextStyle.copyWith(fontSize: 13, fontWeight: medium),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 10,
                ),
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                    color: subTextColor,
                  ),
                  color: transparentColor,
                ),
                child: Text(
                  'Lifestyle',
                  style:
                      subTextStyle.copyWith(fontSize: 13, fontWeight: medium),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 10,
                ),
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                    color: subTextColor,
                  ),
                  color: transparentColor,
                ),
                child: Text(
                  'Kesehatan',
                  style:
                      subTextStyle.copyWith(fontSize: 13, fontWeight: medium),
                ),
              ),
            ],
          ),
        ),
      );
    }

    Widget beritaPopulerTitle() {
      return Container(
        margin: EdgeInsets.only(
          top: 30,
          left: 30,
          right: 30,
        ),
        child: Text(
          'Berita Populer',
          style: primaryTextStyle.copyWith(
            fontSize: 22,
            fontWeight: semiBold,
          ),
        ),
      );
    }

    Widget beritaPopuler() {
      return Container(
        margin: EdgeInsets.only(top: 14),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              SizedBox(
                width: 30,
              ),
              Row(
                children: [
                  BeritaPopuler(),
                  BeritaPopuler(),
                  BeritaPopuler(),
                  BeritaPopuler(),
                ],
              )
            ],
          ),
        ),
      );
    }

    Widget beritaTerbaruTitle() {
      return Container(
        margin: EdgeInsets.only(
          top: 30,
          left: 30,
          right: 30,
        ),
        child: Text(
          'Berita Terbaru',
          style: primaryTextStyle.copyWith(
            fontSize: 22,
            fontWeight: semiBold,
          ),
        ),
      );
    }

    Widget beritaTerbaru() {
      return Container(
        margin: EdgeInsets.only(top: 14),
        child: Column(
          children: [
            BeritaTerbaru(),
            BeritaTerbaru(),
            BeritaTerbaru(),
            BeritaTerbaru(),
            BeritaTerbaru(),
            BeritaTerbaru(),
          ],
        ),
      );
    }

    return ListView(
      children: [
        header(),
        kategori(),
        beritaPopulerTitle(),
        beritaPopuler(),
        beritaTerbaruTitle(),
        beritaTerbaru(),
      ],
    );
  }
}
