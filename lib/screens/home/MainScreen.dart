import 'package:flutter/material.dart';
import 'package:tugasakhir/theme.dart';
import 'ChatScreen.dart';
import 'HomeScreen.dart';
import 'FavoriteScreen.dart';
import 'ProfileScreen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    Widget homeButton() {
      return FloatingActionButton(
        onPressed: () {},
        backgroundColor: secondaryColor,
        child: Image.asset(
          'assets/icons/Home.png',
          color: Colors.white,
          width: 20,
        ),
      );
    }

    Widget customBottomBar() {
      return ClipRRect(
        borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
        child: BottomAppBar(
          shape: CircularNotchedRectangle(),
          notchMargin: 8,
          clipBehavior: Clip.antiAlias,
          child: BottomNavigationBar(
            backgroundColor: backgroundColor4,
            currentIndex: currentIndex,
            onTap: (value) {
              print(value);
              setState(() {
                currentIndex = value;
              });
            },
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                icon: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Image.asset(
                    'assets/icons/Home.png',
                    width: 20,
                    color: currentIndex == 0 ? primaryColor : Color(0xff808191),
                  ),
                ),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Image.asset(
                    'assets/icons/chat.png',
                    width: 20,
                    color: currentIndex == 1 ? primaryColor : Color(0xff808191),
                  ),
                ),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Image.asset(
                    'assets/icons/favorite.png',
                    width: 20,
                    color: currentIndex == 2 ? primaryColor : Color(0xff808191),
                  ),
                ),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Image.asset(
                    'assets/icons/profile.png',
                    width: 20,
                    color: currentIndex == 3 ? primaryColor : Color(0xff808191),
                  ),
                ),
                label: '',
              ),
            ],
          ),
        ),
      );
    }

    Widget body() {
      switch (currentIndex) {
        case 0:
          return HomeScreen();
          break;
        case 1:
          return ChatScreen();
          break;
        case 2:
          return FavoriteScreen();
          break;
        case 3:
          return ProfileScreen();
          break;
        default :
          return HomeScreen();
      }
    }

    return Scaffold(
      backgroundColor: backgroundColor1,
      floatingActionButton: homeButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: customBottomBar(),
      body: body(),
    );
  }
}
