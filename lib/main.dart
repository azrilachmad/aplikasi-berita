import 'package:flutter/material.dart';
import 'package:tugasakhir/screens/GetStarted.dart';
import 'package:tugasakhir/screens/LoginScreen.dart';
import 'package:tugasakhir/screens/SplashScreen.dart';
import 'package:tugasakhir/screens/SignUpScreen.dart';
import 'package:tugasakhir/screens/home/MainScreen.dart';
import 'package:tugasakhir/widgets/DetailBerita.dart';
import 'package:firebase_core/firebase_core.dart';
 
Future <void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => SplashScreen(),
        '/get-started': (context) => GetStarted(),
        '/sign-in': (context) => LoginScreen(),
        '/sign-up': (context) => SignUpScreen(),
        '/home': (context) => MainScreen(),
        '/detail-berita': (context) => DetailBerita(),
      },
    );
  }
}
